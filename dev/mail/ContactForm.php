<?php
class ContactForm {

	public function get_template_html( $template_name, $attr = null ) {
		if ( ! $attr ) {
			$attr = array();
		}
		ob_start();
		require_once($template_name . '.php');
		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	private function send_mail_SMTP ($infor) {

		$nome = $infor['nome'];

		$response =  array();

		$mail = new PHPMailer();
		$mail->CharSet = "UTF-8";
		$mail->IsSMTP();
		$mail->Host = "smtp.joylife.com.br";
		$mail->SMTPAuth = true;
		$mail->Username = "noreply@joylife.com.br";
		$mail->Password = "pass";
		$mail->From = "noreply@joylife.com.br";
		$mail->FromName = "NOREPLY - [".$nome."]";
		$mail->AddAddress('mail', "nome");
		$mail->AddBCC('mail', 'nome'); // Cópia Oculta

		if ($infor['action'] === 'newsletter') {
			$mail->Subjec = '[Cadastro Newsletter]';
		}else {
			$mail->Subjec = '[Contato via Site]';
		}
		$mail->Body = self::get_template_html('body_mail', $infor);

		//enviando e retornando o status de envio
		if ($mail->Send()) {
			$response['success'] = 'Formulário Enviado com Sucesso.<br> Aguarde nosso retorno';
		} else {
			$response['error'] = 'Erro no Envio.<br>Favor tentar mais tarde.';
		}
	}

	private function send_mail ($infor) {

		$response =  array();

		if ($infor['action'] === 'newsletter') {
			$subject = '[Cadastro Newsletter]';
		}else {
			$subject = '[Contato via Site]';
		}

		$email_to = '<contato@joylife.com.br>';

		$name    = stripslashes(trim($infor['nome']));
	    $email   = stripslashes(trim($infor['email']));
	    $message = stripslashes(trim($infor['mensagem']));

		$headers  = "MIME-Version: 1.1" . PHP_EOL;
		$headers .= "Content-type: text/html; charset=utf-8" . PHP_EOL;
		$headers .= "Content-Transfer-Encoding: 8bit" . PHP_EOL;
		$headers .= "Date: " . date('r', $_SERVER['REQUEST_TIME']) . PHP_EOL;
		$headers .= "Message-ID: <" . $_SERVER['REQUEST_TIME'] . md5($_SERVER['REQUEST_TIME']) . '@' . $_SERVER['SERVER_NAME'] . '>' . PHP_EOL;
		$headers .= "From: " . "=?UTF-8?B?".base64_encode($name)."?=" . "<$email>" . PHP_EOL;
		$headers .= "Return-Path: $emailTo" . PHP_EOL;
		$headers .= "Reply-To: $email" . PHP_EOL;
		$headers .= "X-Mailer: PHP/". phpversion() . PHP_EOL;
		$headers .= "X-Originating-IP: " . $_SERVER['SERVER_ADDR'] . PHP_EOL;

		$body = self::get_template_html('body_mail', $infor);

		if( mail($email_to, "=?utf-8?B?".base64_encode($subject)."?=", $body, $headers) ) {
			if ($infor['action'] === 'newsletter') {
				$response['success'] = 'Seu cadastro foi realizado com sucesso';
			}else {
				$response['success'] = 'Formulário Enviado com Sucesso. Aguarde nosso retorno';
			}
		} else {
		    $response['error'] = 'Erro no Envio. Favor tentar mais tarde';
		}

		return $response;
	}

	public function action ($infor) {

		$response =  array();

		if ($infor['action'] === 'contact') {

			$infor['title'] = "Formulário de Contato";
			$response = self::send_mail($infor);

		} elseif ($infor['action'] === 'newsletter') {

			$infor['title'] = "Cadastro Newsletter";
			$response = self::send_mail($infor);

		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

}
?>
