<?php include('header.php') ?>

<div class="banner-internas">
    <div class="banner-tratamentos"></div>
</div>

<main>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Tratamentos</h1>
                </div>
            </div>
            <div class="row tratamentos-box">
                <div class="col-md-6">
                    <a href="orto.php" class="overlay--active">
                        <div class="overlay-container">
                            <img src="img/orto.jpg" alt="">
                            <div class="overlay">
                                <img src="img/rpg.svg" alt="">
                            </div>
                            <h3>Fisioterapia Ortopédica e Traumatológica</h3>
                            <p>
                                Atua na prevenção e no tratamento de distúrbios do sistema musculo-esqueletico. A Fisioterapia ortopédica trata disfunções osteomioarticulares e tendíneas resultantes de traumas e suas consequencias imediatas e tardias, lesões...
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="neuro.php" class="overlay--active">
                        <div class="overlay-container">
                            <img src="img/neuro.jpg" alt="">
                            <div class="overlay">
                                <img src="img/neuro.svg" alt="">
                            </div>
                            <h3>Fisioterapia Neurológica</h3>
                            <p>
                                Tratamento que atua nas doenças que acometem o Sistema Nervoso Central ou Periférico, levando a distúrbios neurológicos, motores e cognitivos....
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row tratamentos-box">
                <div class="col-md-6">
                    <a href="dermato.php" class="overlay--active">
                        <div class="overlay-container">
                            <img src="img/dermato.jpg" alt="">
                            <div class="overlay">
                                <img src="img/lft.svg" alt="">
                            </div>
                            <h3>Fisioterapia Dermatofuncional</h3>
                            <p>
                                Área da fisioterapia que atua na prevenção e recuperação físico-funcional dos distúrbios endócrinos metabólicos, dermatológicos e músculo esqueléticos que afetam direta ou indiretamente a pele...
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="spa.php" class="overlay--active">
                        <div class="overlay-container">
                            <img src="img/spa.jpg" alt="">
                            <div class="overlay">
                                <img src="img/dreno.svg" alt="">
                            </div>
                            <h3>SPA</h3>
                            <p>
                                Terapia de pedras quentes  une manobras de massoterapia com aplicação de termoterapia, em que a condução de calor é transmitida ao corpo através de pedras plutônicas...
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include('footer.php') ?>
