<?php include('header.php') ?>

<div class="banner-internas">
    <div class="banner-sobre"></div>
</div>

<main>
    <section>
        <div class="container espaco">
            <div class="row">
                <article class="col-md-12">
                    <h1>O Espaço</h1>
                    <h2>
                        No espaço Joylife, fisioterapia e spa, possuímos uma estrutura especialmente projetada para o seu conforto e bem-estar.
                    </h2>
                </article>
            </div>
            <div class="row">
                <div class="sobre-galeria">
                    <div>
                        <img src="img/sobre02.png" alt="" class="galeria-sobre">
                    </div>
                    <div>
                        <img src="img/sobre01.png" alt="" class="galeria-sobre">
                    </div>
                    <div>
                        <img src="img/sobre03.png" alt="" class="galeria-sobre">
                    </div>
                    <div>
                        <img src="img/sobre04.png" alt="" class="galeria-sobre">
                    </div>
                    <div>
                        <img src="img/sobre05.png" alt="" class="galeria-sobre">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--<section>-->
        <!--<div class="container">-->
            <!--<div class="row">-->
                <!--<article class="col-md-12">-->
                <!--</article>-->
            <!--</div>-->
        <!--</div>-->
    <!--</section>-->
    <section class="joyce-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Dra. Joyce Drummond</h1>
                    <h2>
                        Sempre apaixonada por fisioterapia, bem-estar e saúde, investiu muito e continua investindo na sua formação
                    </h2>
                    <img src="img/joyce.png" alt="" class="joyce">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <p>
                                Cursou a sua graduação no IBMR, como bolsista já que passou em primeiro lugar no processo seletivo. Iniciou sua trajetória em estágios de referência para aprimorar seus conhecimentos, como ABBR, Hospital do Andaraí, clínicas de estética e cirurgias plásticas, e desde então vem complementando sua formação através da pós graduação, cursos, workshops, congressos e diversas palestras.
                            </p>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <p>
                                A Dra. Joyce começou a se aprimorar ainda na faculdade onde fez o curso de pilates pela ótica das cadeias musculares e drenagem linfática. Logo depois da conclusão de curso fez sua capacitção em RPG, pós graduação em dermato-funcional, manipulação articular e muito mais. Todos esses conhecimentos <strong>habilitaram à Dra. Joyce uma compreensão holística do ser humano</strong>.
                            </p>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <p>
                                Sua trajetória científica e acadêmica intermitente, sempre em busca de novos tratamentos, visa <strong>oferecer um atendimento de excelência para seus pacientes</strong>. A Dra. Joyce atende executivos, empresários e celebridades como Flávia Alessandra, Giovanna Antonelli, Glória Pires, Cleo Pires, Renata Sorrah, Heloisa Perissé, Otaviano Costa, Giullia Costa, e todos que almejam uma renovação para o seu corpo diante da rotina corrida da vida.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="joyce-bio-destaque">
                                Concretizando o sonho de oferecer técnica, requinte e conforto para seus pacientes, construiu com muito amor e dedicação o espaço Joylife, fisioterapia e spa.
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <article class="col-md-12">
                    <div class="row curriculo">
                        <div class="col-md-12">
                            <ul class="sobre-historico-principal">
                                <h1>Currículo</h1>
                                <li>
                                    Formada em fisioterapia pelo IBMR<br> (Instituto Brasileiro de Medicina de Reabilitação) em <em>27/02/2008</em>.
                                </li>
                                <li>
                                    Pós-Graduada em fisioterapia dermato-funcional pela UGF<br> (Universidade Gama Filho) em <em>02/08/2009</em>.
                                </li>
                                <li>
                                    RPG/RPM – Reeducação Postural Global, pelo reequilíbrio proprioceptivo e muscular<br> pela CBF (Centro Brasileiro de Fisioterapia) em <em>19/02/2009</em>.
                                </li>
                            </ul>
                            <ul class="sobre-historico-lista">
                                <h3 class="fancy">
                                    <span>2007</span>
                                </h3>
                                <li>
                                    Curso de massagem estética modeladora pelo ABIC em <em>26/05/2007</em>.
                                </li>
                                <li>
                                    Curso de drenagem linfática pelo IBETED em <em>24/07/2007</em>.
                                </li>
                                <li>
                                    Curso de Pilates sob a ótica das cadeias musculares pelo Núcleo Ângela Beatriz Varella em <em>31/08/2007</em>.
                                </li>
                            </ul>
                            <ul class="sobre-historico-lista">
                                <h3 class="fancy">
                                    <span>2008</span>
                                </h3>
                                <li>
                                    Curso de Shiatsuterapia pelo ABIC em <em>08/03/2008</em>.
                                </li>
                                <li>
                                    Curso de Eletroterapia aplicada a estética corporal pelo Associação brasileira de crochetagem em <em>24/04/2008</em>.
                                </li>
                                <li>
                                    Curso de Auriculoterapia em ABIC em <em>11/05/2008</em>.
                                </li>
                                <li>
                                    Aperfeiçoamento em Bambuterapia pela Adcos em <em>18/09/2008</em>.
                                </li>
                                <li>
                                    Curso de Crochetagem pelo Associação Brasileira de Crochetagem em <em>10/10/2008</em>.
                                </li>
                            </ul>
                            <ul class="sobre-historico-lista">
                                <h3 class="fancy">
                                    <span>2009</span>
                                </h3>
                                <li>
                                    Curso de Terapia tridimensional básica em <em>14/03/2009</em>.
                                </li>
                            </ul>
                            <ul class="sobre-historico-lista">
                                <h3 class="fancy">
                                    <span>2010</span>
                                </h3>
                                <li>
                                    Seminário de Pompage em Núcleo Ângela Beatriz Varella em <em>03-05/06/2010</em>.
                                </li>
                            </ul>
                            <ul class="sobre-historico-lista">
                                <h3 class="fancy">
                                    <span>2011</span>
                                </h3>
                                <li>
                                    Curso de Manipulação articular em Associação Brasileira de Crochetagem em <em>31/10/2011</em>.
                                </li>
                            </ul>
                            <ul class="sobre-historico-lista">
                                <h3 class="fancy">
                                    <span>2012</span>
                                </h3>
                                <li>
                                    Curso projeto convergências pelo Centro de São Paulo em <em>09/12/2012</em>.
                                </li>
                            </ul>
                            <ul class="sobre-historico-lista">
                                <h3 class="fancy">
                                    <span>2015</span>
                                </h3>
                                <li>
                                    Curso de abordagem terapêutica e preventiva de fibroses e aderências em cirurgia plástica,<br>LTF (liberação tecidual funcional) em <em>07-08/05/2015</em>.
                                </li>
                                <li>
                                    Nono encontro internacional de fisioterapia dermato-funcional em Belo Horizonte em <em>08/02/2015</em>.
                                </li>
                            </ul>
                            <ul class="sobre-historico-lista">
                                <h3 class="fancy">
                                    <span>2016</span>
                                </h3>
                                <li>
                                    Curso drenodetox método Nany Mota em <em>04/06/2016</em>.
                                </li>
                                <li>
                                    Linfoterapeuta, especializada em drenagem linfática - Formada pela escola de drenagem de Bruxelas.
                                </li>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>
    <a id="leduc"></a>
    <section>
        <div class="container">
            <div class="row">
                <article class="col-md-12">
                    <div class="row curriculo">
                        <div class="col-md-12 leduc">
                            <h1>Curso Internacional de Drenagem Linfática</h1>
                            <div class="col-md-6">
                                <img src="img/leduc01.jpg" alt="">
                            </div>
                            <div class="col-md-6">
	                            <img src="img/leduc02.jpg" alt="">
                            </div>
	                        <div class="col-md-12">
		                        <p>
                                    A Dra. Joyce Drummond está mais uma vez presenteando seus clientes, aprimorando seus conhecimentos através do Curso Oficial Leduc, com o Dr. Albert Leduc e Dr. Olivier Leduc, criadores do conceituado método. Formada Linfoterapeuta, especializada em drenagem linfática pela escola de drenagem de Bruxelas.
                                </p>
	                        </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>
</main>

<?php include('footer.php') ?>