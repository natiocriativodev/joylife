<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Joylife Spa & Fisioterapia</title>
    <!--Meta Mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Meta SEO-->
    <meta name="description" content="Joylife Spa & Fisioterapia, por uma vida mais alegre e disposta!">
    <meta name="keywords" content="Joylife, Joyce Drummond, Fisioterapia, Spa, R.P.G, L.T.F, Crochetagem, Drenagem linfática, Terapia com pedras, Massagem modeladora, Drenodetox, Carboxiterapia, Corrente russa, Recreio, Barra da Tijuca">
    <meta name="author" content="Joylife">
    <meta name="web_author" content="Natio Criativo">
    <!--Social: Facebook-->
    <meta property="og:locale" content="pt_BR">
    <meta property="og:url" content="http://www.joylife.com.br/">
    <meta property="og:title" content="Joylife Spa & Fisioterapia, por uma vida mais alegre e disposta!">
    <meta property="og:site_name" content="Joylife">
    <meta property="og:description" content="Joylife Spa & Fisioterapia, por uma vida mais alegre e disposta">
    <meta property="og:image" content="www.joylife.com.br/imagem.jpg">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">
    <meta property="og:type" content="website">
    <!-- Social: Google+ / Schema.org  -->
    <meta itemprop="name" content="joylife">
    <meta itemprop="description" content="Joylife Spa & Fisioterapia, por uma vida mais alegre e disposta">
    <meta itemprop="image" content="">
    <!--Favicon-->
    <link rel="icon" href="img/favicon.png">
    <!--Folhas de estilo-->
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<header>
    <div class="nav">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6 nav-brand">
                    <a href="index.php">
                        <img src="img/logo.svg" alt="Joylife SPA & Fisioterapia">
                    </a>
                </div>
                <nav class="col-md-9 col-sm-6 col-xs-6">
                    <ul class="nav-menu">
                        <button class="nav-btn--fechar"><i class="mdi mdi-window-close"></i></button>
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <span class="nav-menu-divide">|</span>
                        <li>
                            <a href="sobre.php">Sobre</a>
                        </li>
                        <span class="nav-menu-divide">|</span>
                        <li>
                            <a href="tratamentos.php">Tratamentos</a>
                        </li>
                        <span class="nav-menu-divide">|</span>
                        <li>
                            <a href="contato.php">Contato</a>
                        </li>
                    </ul>
                    <button class="nav-btn--abrir"><i class="mdi mdi-menu"></i></button>
                </nav>
            </div>
        </div>
    </div>
</header>