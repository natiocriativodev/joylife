<?php include('header.php') ?>

<div class="banner-internas">
    <div class="banner-tratamentos"></div>
</div>

<main>
    <section class="tratamentos">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="tratamentos.php" class="voltar-interna"><i class="mdi mdi-chevron-left"></i>Voltar</a>
                    <h1>Fisioterapia neurológica</h1>
                    <p>
                        Atua nas doenças que acometem o Sistema Nervoso Central ou Periférico, levando a distúrbios neurológicos, motores e cognitivos. O objetivo é <strong>avaliar os déficits funcionais</strong> e, através de exercícios direcionados, <strong>promover padrões motores adequados</strong>, <strong>melhorando a força, a coordenação motora e o equilíbrio</strong>.
                    </p>
                    <p>
                        Previne deformidades e otimiza as funções preservadas, com o principal objetivo de proporcionar maior funcionalidade, independência e melhor qualidade de vida para os pacientes e familiares.
                    </p>
                    Indicações:
                    <ul class="list-default">
                        <li>
                            <i class="mdi mdi-checkbox-marked-circle-outline"></i>
                            Acidente Vascular Encefálico (AVE)
                        </li>
                        <li>
                            <i class="mdi mdi-checkbox-marked-circle-outline"></i>
                            Trauma Crânio-Encefálico (TCE)
                        </li>
                        <li>
                            <i class="mdi mdi-checkbox-marked-circle-outline"></i>
                            Lesão Medular (LM)
                        </li>
                        <li>
                            <i class="mdi mdi-checkbox-marked-circle-outline"></i>
                            Mal de Parkinson
                        </li>
                        <li>
                            <i class="mdi mdi-checkbox-marked-circle-outline"></i>
                            Esclerose Múltipla (EM)
                        </li>
                        <li>
                            <i class="mdi mdi-checkbox-marked-circle-outline"></i>
                            Paralisia Cerebral (PC)
                        </li>
                        <li>
                            <i class="mdi mdi-checkbox-marked-circle-outline"></i>
                            Outras doenças que acometam o SNC ou Periférico
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include('footer.php') ?>
