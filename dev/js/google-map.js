
var map;
window.initMap = function() {
	var content_infor = '';
	var position = {lat: -23.005821, lng: -43.437365};

	content_infor += '<div class="content-inforwindow">';
	content_infor += '<div class="content-thumb">';
	content_infor += '<img src="http://clientes.natiocriativo.com/joylife/img/logo.svg">';
	content_infor += '</div>';
	content_infor += '<div class="content-infor">';
	content_infor += '<p>Avenida das Américas, 12.900</p>';
	content_infor += '<p>Condomínio Américas Avenue</p>';
	content_infor += '<p>Ala Brasil | Bloco 1, sala 301.</p>';
	content_infor += '</div>';
	content_infor += '</div>';

	map = new google.maps.Map(document.getElementById('map'), {
		scrollwheel: false,
		center: position,
		zoom: 16
	});

	var infowindow = new google.maps.InfoWindow({
		content: content_infor
	});

	var marker = new google.maps.Marker({
		position: position,
		map: map,
		title: 'JoyLife',
		icon: 'http://clientes.natiocriativo.com/joylife/img/pin-joylife.png'
	});

	// marker.addListener('click', function() {
	//     infowindow.open(map, marker);
	// });

};
