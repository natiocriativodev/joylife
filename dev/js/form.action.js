var FORM_ACTION = (function ($, window, document, undefined) {
    'use strict';
    var form = {} || FORM_ACTION;

    form.contact_form = function() {
        var parameter = {};
        $('#form-contact').on('submit', function(event){
            event.preventDefault();
            event.stopPropagation();
            var values = $(this).serializeArray();
            $.each(values, function(index, field){
				parameter[field.name] = field.value;
			});
			ajax_action($(this), parameter);
        });
    };

    form.news_form = function() {
        var parameter = {};
        $('#form-newsletter').on('submit', function(event){
            event.preventDefault();
            event.stopPropagation();
            var $this = $(this);
            var values = $(this).serializeArray();
            $.each(values, function(index, field){
                parameter[field.name] = field.value;
            });
            ajax_action($this, parameter);
        });
    };

    var ajax_action = function($form, data_serialize){
        var type, titulo, texto = [];
        $.ajax({
            type: 'POST',
            url: 'http://clientes.natiocriativo.com/joylife/mail/request.action.php',
            data: data_serialize,
            beforeSend: function() {
                $form.append("<div class='loading'></div>");
            },
            error: function(jqXHR, textStatus, errorThrown ) {
                type = 'error';
                titulo = 'Erro!';
                texto = textStatus;
            },
            success: function(result){
                if(result.success !== undefined) {
                    console.log(result.success);
                    type = 'success';
                    titulo = 'Sucesso';
                    texto = result.success;
                }else if (result.error !== undefined){
                    type = 'error';
                    titulo = 'Ops!';
                    texto = result.error;
                }
            },
            complete: function(jqXHR, textStatus ) {
                $form.find('.loading').remove();
                var html = '';
                html += '<div class="contato-header">';
                    html += '<h3 class="fancy"><span>'+titulo+'</span></h3>';
                html += '</div>'
                html += '<div>';
                    html += '<p>'+texto+'</p>';
                html += '</div>';
                $('#modal-alert').find('.md-content').addClass('alert-'+type).find('.inner').html(html);
                $("[data-modal='modal-alert']").trigger('click');
            }
        });
    };
    return form;
}( jQuery, window, document ));

FORM_ACTION.contact_form();
FORM_ACTION.news_form();
