$(document).ready(function(){

    // Slider Principal
    $('.slider-principal').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: true,
        dots: true,
        speed: 2000
    });

    $('.depoimentos-slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
        dots: true,
        speed: 2000
    });

    // Historia
    $('.sobre-galeria').slick({
        autoplay: false,
        arrows: true,
        dots: false,
        speed: 1000,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: true,
                    dots: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    dots: false
                }
            }
        ]
    });

    // Botao scroll top
    $(function() {
        $(window).scroll(function() {
            if($(this).scrollTop() != 0) {
                $('.btn-back').fadeIn();   }
            else { $('.btn-back').fadeOut(); }
        });
        $('.btn-back').click(function() {
            $('body,html').animate({scrollTop:0},800);
        });
    });

    // Menu responsivo
    document.querySelector('.nav-btn--abrir').onclick = function() {
        document.documentElement.classList.add('menu-ativo');
    };

    document.querySelector('.nav-btn--fechar').onclick = function() {
        document.documentElement.classList.remove('menu-ativo');
    };

    //Scroll to (Anchor)
    $('.menu-tratamentos, .menu-tratamentos-spa, .menu-tratamentos-orto').on('click', 'a[href^="#"]', function (e) {
        e.preventDefault();
        var target = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(target).offset().top -25
        }, 2000);
    });

    //Fixed sidebar after scroll
    var exeStick = function () {
        if (window.innerWidth > 992 ) {
            $("#sidebar").stick_in_parent();
        }
    };
    exeStick();

    //Active menu item with current url
    $(function() {
        var pgurl = window.location.href.substr(window.location.href
                .lastIndexOf("/")+1);
        $(".nav-menu > li > a").each(function(){
            if($(this).attr("href") == pgurl || $(this).attr("href") == '' )
                $(this).addClass("nav-active");
        });
    });
});